var express = require('express');
var passport = require('passport');
var router = express.Router();

//get environment variables
var env = {
  AUTH0_CLIENT_ID: process.env.AUTH0_CLIENT_ID,
  AUTH0_DOMAIN: process.env.AUTH0_DOMAIN,
  AUTH0_CALLBACK_URL: process.env.AUTH0_CALLBACK_URL || 'http://localhost:3000/callback'
}

// GET home page.
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express', env: env });
});

// GET login page.
router.get('/login',
  function(req, res){
    res.render('login', { env: env });
  });

// GET logout page.
router.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

// GET callback from the authentication (if fails, redirect)
router.get('/callback',
  passport.authenticate('auth0', { failureRedirect: '/url-if-something-fails' }),
  function(req, res) {
    res.redirect(req.session.returnTo || '/user');
  });


module.exports = router;
