var express = require('express');
var passport = require('passport');
var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn()
var router = express.Router();
var db = require('./../db');

// GET user profile.
router.get('/', ensureLoggedIn, function(req, res, next) {
    db.init('users')
        .then(function(){
            console.log(req.user);
            db.insert_doc(req.user._json, 'users');
        })
        .catch(function(error) {
            console.log(error);
        })
    res.render('user', { user: req.user });
});

module.exports = router;
